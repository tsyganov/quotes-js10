import React from 'react';
import {MenuItem} from "@material-ui/core";
import {Link, useParams} from "react-router-dom";
import {CATEGORIES} from "../../constants";

const CategoryMenu = () => {
  const {category} = useParams(); // {category: 'star-wars'}

  return (
    <>
      <MenuItem
        component={Link}
        to={'/'}
        selected={!category}
      >
        All
      </MenuItem>
      {CATEGORIES.map(c => (
        <MenuItem
          selected={c.id === category}
          key={c.id}
          component={Link}
          to={'/quotes/' + c.id}
        >
          {c.title}
        </MenuItem>
      ))}
    </>
  );
};

export default CategoryMenu;