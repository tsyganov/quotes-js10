import React from 'react';
import {makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  title: {
    textAlign: 'center',
    marginTop: theme.spacing(2) // 16px 1 unit = 8 px
  }
}));

const NotFound = () => {
  const classes = useStyles();

  return (
    <Typography variant="h4" className={classes.title}>Not found</Typography>
  );
};

export default NotFound;