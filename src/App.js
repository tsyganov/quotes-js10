import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import NotFound from "./components/NotFound/NotFound";
import {Container} from "@material-ui/core";
import AddQuote from "./pages/AddQuote/AddQuote";
import Quotes from "./pages/Quotes/Quotes";
import EditQuote from "./pages/EditQuote/EditQuote";

const App = () => (
  <Layout>
    <Container>
      <Switch>
        <Route path="/" exact component={Quotes}/>
        <Route path="/quotes/edit/:id" component={EditQuote}/>
        <Route path="/quotes/:category" component={Quotes}/>
        <Route path="/add-quote" component={AddQuote}/>
        <Route component={NotFound}/>
      </Switch>
    </Container>
  </Layout>
);

export default App;
