import React, {useCallback, useEffect, useState} from 'react';
import {Drawer, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {CATEGORIES} from "../../constants";
import axiosApi from "../../axiosApi";
import Quote from "../../components/Quote/Quote";
import CategoryMenu from "../../components/CategoryMenu/CategoryMenu";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
    paddingTop: theme.spacing(1),
  },
  content: {
    flexGrow: 1,
    paddingTop: theme.spacing(2),
  },
}));

const Quotes = ({match}) => {
  const classes = useStyles();
  const [quotes, setQuotes] = useState([]);
  const category = match.params.category;

  const fetchData = useCallback(async () => {
    let url = '/quotes.json';

    if (category) {
      url += `?orderBy="category"&equalTo="${category}"`
    }

    const response = await axiosApi.get(url);

    const quotes = Object.keys(response.data).map(id => ({
      ...response.data[id],
      id,
    }));

    setQuotes(quotes);
  }, [category]);

  useEffect(() => {
    fetchData().catch(console.error);
  }, [fetchData]);

  const getCategoryName = () => {
    if (!category) {
      return 'All';
    }

    const categoryObj = CATEGORIES.find(c => c.id === category);
    return categoryObj ? categoryObj.title : 'Not found';
  };

  const removeQuote = async id => {
    await axiosApi.delete(`/quotes/${id}.json`);
    await fetchData();
  };

  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar/>
        <div className={classes.drawerContainer}>
          <CategoryMenu />
        </div>
      </Drawer>
      <main className={classes.content}>
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <Typography variant="h4">
              {getCategoryName()}
            </Typography>
          </Grid>

          {quotes.map(quote => (
            <Quote
              key={quote.id}
              id={quote.id}
              author={quote.author}
              text={quote.text}
              remove={() => removeQuote(quote.id)}
            />
          ))}
        </Grid>

      </main>
    </div>
  );
};

export default Quotes;